#!/usr/bin/env bash

base_folder="/home/cubie/piwigo/scripts"
python_script="${base_folder}/createThumbnails.py"

log="${base_folder}/thumb.log"
errlog="${base_folder}/thumb.err"

cd /home/cubie/piwigo/config/www/gallery/galleries
nice $python_script
# >>$log 2>$errlog
