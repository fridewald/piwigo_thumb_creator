#!/usr/bin/env python3

import os
import time
import sys
import functools
import glob
import fcntl
import errno
import logging
import multiprocessing

from PIL import Image
from multiprocessing import Pool


# just copied from https://stackoverflow.com/questions/4228530/pil-thumbnail-is-rotating-my-image
def image_transpose_exif(im):
    """
        Apply Image.transpose to ensure 0th row of pixels is at the visual
        top of the image, and 0th column is the visual left-hand side.
        Return the original image if unable to determine the orientation.

        As per CIPA DC-008-2012, the orientation field contains an integer,
        1 through 8. Other values are reserved.
    """

    exif_orientation_tag = 0x0112
    exif_transpose_sequences = [                   # Val  0th row  0th col
        [],                                        # 0    (reserved)
        [],                                        # 1   top      left
        [Image.FLIP_LEFT_RIGHT],                   # 2   top      right
        [Image.ROTATE_180],                        # 3   bottom   right
        [Image.FLIP_TOP_BOTTOM],                   # 4   bottom   left
        [Image.FLIP_LEFT_RIGHT, Image.ROTATE_90],  # 5   left     top
        [Image.ROTATE_270],                        # 6   right    top
        [Image.FLIP_TOP_BOTTOM, Image.ROTATE_90],  # 7   right    bottom
        [Image.ROTATE_90],                         # 8   left     bottom
    ]

    try:
        seq = exif_transpose_sequences[im._getexif()[exif_orientation_tag]]
    except Exception:
        return im
    else:
        return functools.reduce(type(im).transpose, seq, im)


class thumbnails:

    def __init__(self, filepath, thumb_base):
        self.filepath = filepath
        self.thumb_base = thumb_base
        self.im = 0
        self.logger = logging.getLogger()

    def read_image(self):
        if self.im == 0:
            self.im = Image.open(self.filepath)
            self.im = image_transpose_exif(self.im)
            # prevent distortion
            self.im2 = self.im.copy()

    def create(self):
        # filename and extension
        file_base, ext = os.path.splitext(self.filepath)
        # create the thumbnail folder
        thumb_folder = self.thumb_base + os.path.split(self.filepath)[0]
        # basepath = "./thumbnails/"

        if not os.path.isdir(thumb_folder):
            os.makedirs(thumb_folder)

        try:
            # thumb it up
            # create the thumbnails if none exist
            n_thumb = 0
            # crop an image to square
            n_thumb += self.check_create_square((120, 120), self.thumb_base + file_base + '-sq' + ext)
            n_thumb += self.check_create_thumbnails((1224, 918), self.thumb_base + file_base + '-xl' + ext)
            n_thumb += self.check_create_thumbnails((1008, 756), self.thumb_base + file_base + '-la' + ext)
            n_thumb += self.check_create_thumbnails((792, 594), self.thumb_base + file_base + '-me' + ext)
            n_thumb += self.check_create_thumbnails((576, 432), self.thumb_base + file_base + '-sm' + ext)
            n_thumb += self.check_create_thumbnails((432, 324), self.thumb_base + file_base + '-xs' + ext)
            n_thumb += self.check_create_thumbnails((240, 240), self.thumb_base + file_base + '-2s' + ext)
            n_thumb += self.check_create_thumbnails((144, 144), self.thumb_base + file_base + '-th' + ext)
            #self.logger.info("Generated %d thumbnails for %s", n_thumb, filepath)
            if n_thumb is 0:
                pass
            else:
                print("Generated {} thumbnails for {}".format(n_thumb, self.filepath))

            return 'OK'
        except Exception as e:
            return e


    def check_create_thumbnails(self, size, path):
        """
        check and balance
        check if file under path already exists
        if not create a thumbnail
        """
        if not os.path.isfile(path):
            self.read_image()
            x, y = self.im.size
            if x > size[0] or y > size[1]:
                self.im.thumbnail(size)
                self.im.save(path)
                # switch
                # tmp = self.im2
                # self.im2 = self.im
                # self.im = tmp
                return 1
        return 0


    def check_create_square(self, size, path):
        """crop image to square"""
        if not os.path.isfile(path):
            self.read_image()
            x, y = self.im2.size
            if x > y:
                c = x - y
                box = (round(c/2.), 0, round(x - c/2.), y)
            else:
                c = y - x
                box = (0, round(c/2.), x, round(y - c/2.))
            crop = self.im2.crop(box)
            crop.thumbnail(size)
            crop.save(path)
            return 1
        return 0

def do_thumb(filename, thumb_base):
    thumb = thumbnails(filename, thumb_base)
    thumb.create()

if __name__=="__main__":
    # flock file name
    flock_name = "./thumbs.lock"
    # image extensions
    extensions = ("jpg", "JPG", "jpeg", "JPEG", "PNG", "png") #, "gif", "GIF")

    # path to thumbnail root
    thumb_base = "/home/cubie/piwigo/config/www/gallery/_data/i/galleries/"

    # create partial function for pool.map
    thumber = functools.partial(do_thumb, thumb_base=thumb_base)
    # number of cpu cores
    n_cores = 2
    # logging stuff
    logger = multiprocessing.get_logger()
    logging.basicConfig(filename="/home/cubie/piwigo/scripts/thumb.log", level=logging.INFO)

    # has to be below logger
    pool = Pool(n_cores)


    with open(flock_name, 'w') as flock:
        try:
            fcntl.lockf(flock, fcntl.LOCK_EX | fcntl.LOCK_NB)

            # find all image files
            files = []
            for ext in extensions:
                files.extend(glob.glob('./**/*.' + ext, recursive=True))

            # files = glob.glob(sys.argv[1], recursive=True)

            results = pool.map(thumber, files)
        except OSError as e:
            if e.errno == errno.EAGAIN:
                sys.stderr.write('Script is already running\n')
                sys.exit(-1)
